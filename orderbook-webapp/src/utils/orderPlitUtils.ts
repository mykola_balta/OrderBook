import { PlotData } from "plotly.js";
import { Order } from "../types.ts/types";

export const getTraceBids = (bids: Order[]): Partial<PlotData> => ({
    x: bids.map((bid) => bid.price),
    y: bids.map((bid) => bid.size),
    type: 'bar',
    name: 'Bids',
    marker: { color: 'green' }
});

export const getTraceAsks = (asks: Order[]): Partial<PlotData> => ({
    x: asks.map((ask) => ask.price),
    y: asks.map((ask) => ask.size),
    type: 'bar',
    name: 'Asks',
    marker: { color: 'red' }
});
export const getTrace = (bids: Order[], asks: Order[]): {
    bids: Partial<PlotData>;
    asks: Partial<PlotData>;
} => {
    return {
        bids: {
            x: bids.map((bid) => bid.price),
            y: bids.map((bid) => bid.size),
            type: 'bar',
            name: 'Bids',
            marker: { color: 'green' }
        },
        asks: {
            x: asks.map((ask) => ask.price),
            y: asks.map((ask) => ask.size),
            type: 'bar',
            name: 'Asks',
            marker: { color: 'red' }
        }
    }
}
export const layout = {
    title: "",
    xaxis: { title: "Price" },
    yaxis: { title: "Volume" },
};