import { Order } from "../types.ts/types";

export const formatCurrency = (value: number): string => {
  return new Intl.NumberFormat("en-US", {
    style: "decimal",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
    useGrouping: true,
  }).format(value);
};

export const sortOrders = (orders: Order[], type: "asc" | "desc"): Order[] => {
  return orders.sort((a, b) =>
    type === "asc" ? a.price - b.price : b.price - a.price
  );
};

export const aggregateOrders = (orders: Order[]): Order[] => {
  const aggregated: { [key: number]: number } = {};
  orders.forEach((order) => {
    if (aggregated[order.price]) {
      aggregated[order.price] += order.size;
    } else {
      aggregated[order.price] = order.size;
    }
  });
  return Object.keys(aggregated).map((price) => ({
    price: parseFloat(price),
    size: aggregated[parseFloat(price)],
  }));
};

export const calculateCumulativeTotals = (orders: Order[]): Order[] => {
  let cumulativeTotal = 0;
  return orders.map((order) => {
    cumulativeTotal += order.size;
    return {
      price: order.price,
      size: cumulativeTotal,
    };
  });
};
