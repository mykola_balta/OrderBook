import CreatableSelect from 'react-select/creatable';
import "./styles.scss";

interface SelectProps {
    onChangeOption: (value?: string) => void;
}
const Select = ({ onChangeOption }: SelectProps) => {
    const colourOptions = [
        { value: 'BTCUSDT', label: 'BTCUSDT', color: '#00B8D9', },
        { value: 'ETHUSDT', label: 'ETHUSDT', color: '#0052CC' },
    ];

    return (
        <div className='select-container'>
            <CreatableSelect isClearable options={colourOptions} onChange={(props) => onChangeOption(props?.value)} />
        </div>
    )
}

export default Select;