import { Routes, Route } from "react-router-dom";
import OrderBookPage from "../../pages/OrderBookPage/OrderBookPage";
import { AppRoutes } from "../../utils/routes";
import { BrowserRouter as Router } from 'react-router-dom';

const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route path={AppRoutes.home} element={<OrderBookPage />} />
      </Routes>
    </Router>
  );
};

export default AppRouter;
