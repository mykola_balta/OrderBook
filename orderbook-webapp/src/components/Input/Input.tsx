import React from 'react';
import "./styles.scss";

interface Props {
  value: string;
  setValue: React.Dispatch<React.SetStateAction<string>>;
  placeholder?: string;
}

const Input: React.FC<Props> = ({ value, setValue, placeholder }) => {
  return (
    <input
      type="text"
      className="inputField"
      value={value}
      onChange={(e) => setValue(e.target.value)}
      placeholder={placeholder}
    />
  );
};

export default Input;
