import {
  HubConnection,
  HubConnectionBuilder,
  LogLevel,
  HttpTransportType,
  IRetryPolicy,
} from "@microsoft/signalr";
import { Order } from "../types.ts/types";

class CustomRetryPolicy implements IRetryPolicy {
  nextRetryDelayInMilliseconds(retryContext: {
    elapsedMilliseconds: number;
    previousRetryCount: number;
    retryReason: any;
  }): number | null {
    if (retryContext.previousRetryCount < 5) {
      return (retryContext.previousRetryCount + 1) * 2000; // delay retries
    }
    return null;
  }
}
const orderBookApiHubUrl = process.env.REACT_APP_ORDERBOOK_API_URL + "/orderbookHub";

export class SignalRService {
  public connection: HubConnection;
  public onOrderBookUpdate: ((asks: Order[], bids: Order[]) => void) | null =
    null;

  constructor() {
    this.connection = new HubConnectionBuilder()
      .withUrl(orderBookApiHubUrl, {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets,
      })
      .configureLogging(LogLevel.Information)
      .withAutomaticReconnect(new CustomRetryPolicy())
      .build();

    this.connection.on(
      "ReceiveOrderBookUpdate",
      (asks: Order[], bids: Order[], symbol: string) => {
        console.log("Received OrderBook Update", asks, bids);
        if (this.onOrderBookUpdate) {
          this.onOrderBookUpdate(asks, bids);
        }
      }
    );

    this.connection.onreconnecting((error) => {
      console.error("Reconnecting due to error:", error);
    });

    this.connection.onreconnected((connectionId) => {
      console.log(`Reconnected. Connection ID: ${connectionId}`);
    });

    this.connection.onclose((error) => {
      console.error("Connection closed:", error);
    });
  }

  public async startConnection() {
    if (this.connection.state === "Disconnected") {
      try {
        await this.connection.start();
        console.log("SignalR Connected.");
      } catch (err) {
        console.error("Error while starting connection: ", err);
        setTimeout(() => this.startConnection(), 5000);
      }
    } else {
      console.warn(
        "Attempted to start connection when SignalR is not disconnected."
      );
    }
  }
}
