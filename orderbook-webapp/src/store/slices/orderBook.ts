import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = {
    bids: [],
    asks: []
}

const orderBookSlice = createSlice({
    name: "orderBookSlice",
    initialState,
    reducers: {
        setOrderBook:  (state, { payload }: PayloadAction<any | null>) => {
            state = payload;
        }
    }
})

export const {setOrderBook} = orderBookSlice.actions;
export default orderBookSlice.reducer;