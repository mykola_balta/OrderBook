import { configureStore } from "@reduxjs/toolkit";
import orderBook from "./slices/orderBook";
import { TypedUseSelectorHook, useSelector } from "react-redux";

export const store = configureStore({
    reducer: {
        orderBook: orderBook
    }
});

export type IRootState = ReturnType<typeof store.getState>
export const useAppSelector: TypedUseSelectorHook<IRootState> = useSelector;
export type AppDispatch = typeof store.dispatch;