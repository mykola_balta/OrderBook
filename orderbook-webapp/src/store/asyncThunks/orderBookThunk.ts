import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const BASE_URL = `${process.env.REACT_APP_ORDERBOOK_API_URL}/api/order-book`;

export const subscribe = createAsyncThunk(
    'orderBookSlice/subscribe',
    async (payload: { symbol: string, exchange?: string, depth?: number }, { rejectWithValue }) => {
        try {
            const { depth = 50, exchange = "ByBit", symbol } = payload;
            await axios({
                method: "post",
                url: `${BASE_URL}/subscribe`,
                data: {
                    depth,
                    exchange,
                    symbol,
                }
            })
        } catch (error: unknown) {
            return rejectWithValue(error);
        }
    });

export const unsubscribe = createAsyncThunk(
    'orderBookSlice/unsubscribe',
    async (payload: { symbol: string, exchange?: string }, { rejectWithValue }) => {
        try {
            const { exchange = "ByBit", symbol } = payload;
            await axios({
                method: "post",
                url: `${BASE_URL}/unsubscribe`,
                data: {
                    exchange,
                    symbol,
                }
            })
        } catch (error: unknown) {
            return rejectWithValue(error);
        }
    })