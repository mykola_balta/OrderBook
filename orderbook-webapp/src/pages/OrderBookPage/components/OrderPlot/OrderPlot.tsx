import React from 'react';
import Plot from 'react-plotly.js';
import { getTrace, layout } from '../../../../utils/orderPlitUtils';
import { Order } from '../../../../types.ts/types';
import "./styles.scss";

interface Props {
  bids: Order[];
  asks: Order[];
}

const OrderPlot: React.FC<Props> = ({ bids, asks }) => {
  const trace = getTrace(bids, asks);

  return <Plot data={[trace.bids, trace.asks]} layout={layout} className='container'/>;
};

export default OrderPlot;
