import React, { useEffect, useState } from "react";
import {
  sortOrders,
  aggregateOrders,
  calculateCumulativeTotals,
  formatCurrency,
} from "../../utils/orderUtils";
import { SignalRService } from "../../services/signalRService";
import OrderPlot from "./components/OrderPlot/OrderPlot";
import Input from "../../components/Input/Input";
import { Order } from "../../types.ts/types";
import "./styles.scss";
import Select from "../../components/Select/Select";
import { useDispatch } from "react-redux";
import { subscribe, unsubscribe } from "../../store/asyncThunks/orderBookThunk";

const BidsAsksChart = () => {
  const [bids, setBids] = useState<Order[]>([]);
  const [asks, setAsks] = useState<Order[]>([]);
  const [service] = useState(new SignalRService());
  const [amount, setAmount] = useState("");
  const [totalCost, setTotalCost] = useState<number | null>(null);

  const dispatch = useDispatch();

  useEffect(() => {
    if (amount && asks.length) {
      calculateCost(parseFloat(amount));
    }
  }, [amount, asks]);

  const calculateCost = (amount: number) => {
    let totalCost = 0;
    let amountRemaining = amount;

    for (const ask of asks) {
      if (amountRemaining <= 0) break;
      if (ask.size >= amountRemaining) {
        totalCost += amountRemaining * ask.price;
        break;
      } else {
        totalCost += ask.size * ask.price;
        amountRemaining -= ask.size;
      }
    }

    setTotalCost(totalCost);
  };

  const processOrders = (incomingOrders: Order[], type: "bid" | "ask") => {
    const sortedOrders = sortOrders(
      incomingOrders,
      type === "bid" ? "desc" : "asc"
    );
    const aggregatedOrders = aggregateOrders(sortedOrders);
    const ordersWithCumulativeTotals =
      calculateCumulativeTotals(aggregatedOrders);

    if (type === "bid") {
      setBids(ordersWithCumulativeTotals);
    } else {
      setAsks(ordersWithCumulativeTotals);
    }
  };

  useEffect(() => {
    service.onOrderBookUpdate = (receivedAsks, receivedBids) => {
      processOrders(receivedBids, "bid");
      processOrders(receivedAsks, "ask");
    };

    service.startConnection().catch(console.error);

    return () => {
      service.connection.stop();
    };
  }, [service]);

  const [selectedCurrency, setSelectedCurrency] = useState<string | null>(null);

  const onChangeCurrency = async (value: undefined | string) => {
    if (!value) {
      return;
    }
    if (selectedCurrency) {
      //@ts-ignore
      await dispatch(unsubscribe({
        symbol: selectedCurrency,
      }))
    }

    //@ts-ignore
    await dispatch(subscribe({
      symbol: value,
    }))

    setSelectedCurrency(value);
  }
  return (
    <div className="chartContainer">
      <Select onChangeOption={onChangeCurrency} />
      <OrderPlot bids={bids} asks={asks} />

      <Input value={amount} setValue={setAmount} placeholder="Enter amount..." />

      {totalCost && (
        <h2 className="totalCostDisplay">
          Total Cost: {formatCurrency(totalCost)}
        </h2>
      )}
    </div>
  );
};

export default BidsAsksChart;
