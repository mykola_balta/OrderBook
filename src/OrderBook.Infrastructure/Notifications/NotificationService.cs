﻿using Microsoft.AspNetCore.SignalR;
using OrderBook.Application.Abstractions;
using OrderBook.Domain.Models;
using OrderBook.Infrastructure.Hubs;

namespace OrderBook.Infrastructure.Notifications
{
    public class NotificationService : INotificationService
    {
        private readonly IHubContext<OrderBookHub> _hubContext;

        public NotificationService(IHubContext<OrderBookHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task NotifyOrderBookUpdate(string exchange, string symbol, List<Order> asks, List<Order> bids)
        {
            var clients = OrderBookHub.GetClientConnections(exchange, symbol).ToList();

            await _hubContext.Clients.All.SendAsync("ReceiveOrderBookUpdate", asks, bids, symbol);

            //TODO: Proper connection handing 
            //foreach (var client in clients)
            //{
            //    await _hubContext.Clients.Client(client).SendAsync("ReceiveOrderBookUpdate", asks, bids, symbol);
            //}
        }
    }
}
