﻿using Microsoft.Extensions.Logging;
using OrderBook.Application.Abstractions;
using System.Net.WebSockets;
using System.Text;

namespace OrderBook.Infrastructure.WebSockets
{
    public class WebSocketService : IWebSocketService, IDisposable
    {
        private ClientWebSocket _webSocket;
        private readonly ILogger<WebSocketService> _logger;
        private bool _disposed;

        public WebSocketService(ILogger<WebSocketService> logger)
        {
            _logger = logger;
        }

        public async Task ConnectAsync(string url, CancellationToken cancellationToken)
        {
            _webSocket = new ClientWebSocket();
            _logger.LogInformation($"Connecting to {url}...");
            await _webSocket.ConnectAsync(new Uri(url), cancellationToken);
            _logger.LogInformation($"Connected to {url}");
        }

        public async Task SendAsync(string message, CancellationToken cancellationToken)
        {
            var bytes = Encoding.UTF8.GetBytes(message);
            var buffer = new ArraySegment<byte>(bytes);

            _logger.LogInformation($"Sending message: {message}");
            await _webSocket.SendAsync(buffer, WebSocketMessageType.Text, true, cancellationToken);
            _logger.LogInformation("Message sent successfully");
        }

        public async Task<string> ReceiveAsync(CancellationToken cancellationToken)
        {
            var buffer = new byte[1024 * 4];
            var result = await _webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), cancellationToken);

            if (result.MessageType == WebSocketMessageType.Close)
            {
                _logger.LogInformation("WebSocket connection closed by the server.");
                await DisconnectAsync(cancellationToken);
                return null;
            }

            var message = Encoding.UTF8.GetString(buffer, 0, result.Count);
            return message;
        }

        public async Task DisconnectAsync(CancellationToken cancellationToken)
        {
            if (_webSocket.State == WebSocketState.Open)
            {
                _logger.LogInformation("Disconnecting WebSocket...");
                await _webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Service stopping", cancellationToken);
                _logger.LogInformation("WebSocket disconnected successfully.");
            }

            _webSocket.Dispose();
            _webSocket = null;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _webSocket?.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
