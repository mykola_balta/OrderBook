﻿using Microsoft.AspNetCore.SignalR;
using OrderBook.Application.Models.Requests;
using OrderBook.Domain.Enums;
using System.Collections.Concurrent;

namespace OrderBook.Infrastructure.Hubs
{
    public class OrderBookHub : Hub
    {
        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<string, SubscribeRequest>> _clientSubscriptions
            = new ConcurrentDictionary<string, ConcurrentDictionary<string, SubscribeRequest>>();

        public async Task Subscribe(string exchange, string symbol, int depth)
        {
            var connectionId = Context.ConnectionId;
            var key = $"{exchange}_{symbol}";

            var subscription = new SubscribeRequest
            {
                Exchange = Enum.Parse<Exchange>(exchange),
                Symbol = symbol,
                Depth = depth
            };

            var subscriptions = _clientSubscriptions.GetOrAdd(connectionId, _ => new ConcurrentDictionary<string, SubscribeRequest>());
            subscriptions[key] = subscription;

            await Clients.Caller.SendAsync("Subscribed", exchange, symbol, depth);
        }

        public async Task Unsubscribe(string exchange, string symbol)
        {
            var connectionId = Context.ConnectionId;
            var key = $"{exchange}_{symbol}";

            if (_clientSubscriptions.TryGetValue(connectionId, out var subscriptions))
            {
                subscriptions.TryRemove(key, out _);

                if (subscriptions.IsEmpty)
                {
                    _clientSubscriptions.TryRemove(connectionId, out _);
                }
            }

            await Clients.Caller.SendAsync("Unsubscribed", exchange, symbol);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _clientSubscriptions.TryRemove(Context.ConnectionId, out _);
            return base.OnDisconnectedAsync(exception);
        }

        public static IEnumerable<string> GetClientConnections(string exchange, string symbol)
        {
            var key = $"{exchange}_{symbol}";
            return _clientSubscriptions
                .Where(kvp => kvp.Value.ContainsKey(key))
                .Select(kvp => kvp.Key);
        }
    }
}
