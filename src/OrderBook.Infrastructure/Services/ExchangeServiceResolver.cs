﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OrderBook.Application.Abstractions;
using OrderBook.Domain.Enums;

namespace OrderBook.Infrastructure.Services
{
    public class ExchangeServiceResolver : IExchangeServiceResolver
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ExchangeServiceResolver> _logger;
        private readonly Dictionary<Exchange, string> _exchangeUrls;

        public ExchangeServiceResolver(ILogger<ExchangeServiceResolver> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;

            _exchangeUrls = Enum.GetValues(typeof(Exchange))
                .Cast<Exchange>()
                .ToDictionary(
                    exchange => exchange,
                    exchange => _configuration[$"ExchangeUrls:{exchange}"]
                );

            // Validate if any exchange URL is missing
            foreach (var exchange in _exchangeUrls)
            {
                if (string.IsNullOrEmpty(exchange.Value))
                {
                    _logger.LogError($"Exchange URL for {exchange.Key} is missing.");
                    throw new ArgumentNullException(nameof(exchange.Value), $"Exchange URL for {exchange.Key} is missing.");
                }
            }

            _logger.LogInformation("Exchange URLs loaded successfully.");
        }

        public string GetWebSocketUrl(Exchange exchange)
        {
            if (_exchangeUrls.TryGetValue(exchange, out var url))
            {
                return url;
            }
            throw new ArgumentException($"Unknown exchange: {exchange}");
        }
    }


}
