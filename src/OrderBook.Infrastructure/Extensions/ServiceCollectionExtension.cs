﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OrderBook.Application.Abstractions;
using OrderBook.Application.Helpers;
using OrderBook.Application.Implementation;
using OrderBook.Infrastructure.Notifications;
using OrderBook.Infrastructure.Services;
using OrderBook.Infrastructure.WebSockets;

namespace OrderBook.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration, bool enableSensitiveLogging = false)
        {

            //services.AddHostedService<ServiceBusConsumerService>();
            //services.AddHostedService<WiseInterviewBackgroundService>();

            services.AddTransient<IWebSocketService, WebSocketService>();
            services.AddScoped<INotificationService, NotificationService>();

            // Register other exchange services as needed
            services.AddSingleton<IExchangeServiceResolver, ExchangeServiceResolver>();
            services.AddSignalR();

            //services.AddSingleton<IWebSocketService, WebSocketService>();
            //services.AddSingleton<IOrderBookStore, OrderBookStore>();
            //services.AddSingleton<IOrderBookUrlProvider, OrderBookUrlProvider>();
            //services.AddSingleton<IOrderBookService, OrderBookService>();
            //services.AddSingleton<OrderBookUpdateHandler>();



            //services.AddSingleton<ConnectionManager>();
            //services.AddSingleton<IMessageHandlerFactory, MessageHandlerFactory>();

            //// Register individual handlers
            //services.AddScoped<GenerateQuestionsHandler>();
            //services.AddScoped<CheckQuestionHandler>();
            //services.AddScoped<GenerateSummaryHandler>();


            return services;
        }
    }
}
