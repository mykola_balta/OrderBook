﻿namespace OrderBook.Domain.Models
{
    public class SubscriptionMessage
    {
        public string ReqId { get; set; }
        public string Op { get; set; }
        public string[] Args { get; set; }

        public SubscriptionMessage(string symbol, int depth)
        {
            ReqId = "test";
            Op = "subscribe";
            Args = new[] { $"orderbook.{depth}.{symbol}" };
        }
    }
}
