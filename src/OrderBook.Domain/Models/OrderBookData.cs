﻿using Newtonsoft.Json;

namespace OrderBook.Domain.Models
{
    public class OrderBookData
    {
        [JsonProperty("topic")]
        public string Topic { get; set; }

        [JsonProperty("ts")]
        public long Timestamp { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("data")]
        public OrderBookDataDetails Data { get; set; }

        [JsonProperty("cts")]
        public long Cts { get; set; }
    }
}
