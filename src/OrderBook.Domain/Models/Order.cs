﻿namespace OrderBook.Domain.Models
{
    public class Order
    {
        public decimal Price { get; set; }
        public decimal Size { get; set; }
    }
}
