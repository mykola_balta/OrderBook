﻿using Newtonsoft.Json;

namespace OrderBook.Domain.Models
{
    public class OrderBookDataDetails
    {
        [JsonProperty("s")]
        public string Symbol { get; set; }

        [JsonProperty("b")]
        public List<List<string>> BidsRaw { get; set; }

        [JsonProperty("a")]
        public List<List<string>> AsksRaw { get; set; }
    }
}
