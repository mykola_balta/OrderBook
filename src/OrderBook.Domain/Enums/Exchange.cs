﻿namespace OrderBook.Domain.Enums
{
    public enum Exchange
    {
        ByBit,
        BitGet,
        Coinbase
    }
}
