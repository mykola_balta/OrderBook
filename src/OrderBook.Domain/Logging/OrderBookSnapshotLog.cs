﻿using OrderBook.Domain.Models;

namespace OrderBook.Domain.Logging
{
    public class OrderBookSnapshotLog
    {
        public string Exchange { get; set; }
        public string Symbol { get; set; }
        public List<Order> Asks { get; set; }
        public List<Order> Bids { get; set; }
        public DateTime Timestamp { get; set; }
    }

}
