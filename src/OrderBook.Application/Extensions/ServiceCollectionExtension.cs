﻿using Microsoft.Extensions.DependencyInjection;
using OrderBook.Application.Abstractions;
using OrderBook.Application.Implementation;

namespace OrderBook.Application.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IOrderBookStore, OrderBookStore>();
            services.AddScoped<IOrderBookService, OrderBookService>();
            services.AddSingleton<ISubscriptionManager, SubscriptionManager>();

            return services;
        }
    }
}
