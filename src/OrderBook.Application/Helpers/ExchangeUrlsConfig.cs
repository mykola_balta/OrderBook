﻿namespace OrderBook.Application.Helpers
{
    public class ExchangeUrlsConfig
    {
        public Dictionary<string, string> ExchangeUrls { get; set; }
    }
}
