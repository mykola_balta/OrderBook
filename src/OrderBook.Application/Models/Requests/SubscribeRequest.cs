﻿using OrderBook.Domain.Enums;

namespace OrderBook.Application.Models.Requests
{
    public class SubscribeRequest
    {
        public Exchange Exchange { get; set; }
        public string Symbol { get; set; }
        public int Depth { get; set; }
    }
}
