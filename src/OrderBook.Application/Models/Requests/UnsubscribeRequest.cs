﻿using OrderBook.Domain.Enums;

namespace OrderBook.Application.Models.Requests
{
    public class UnsubscribeRequest
    {
        public Exchange Exchange { get; set; }
        public string Symbol { get; set; }
    }
}
