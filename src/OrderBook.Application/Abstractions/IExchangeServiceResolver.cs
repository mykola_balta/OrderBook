﻿using OrderBook.Domain.Enums;

namespace OrderBook.Application.Abstractions
{
    public interface IExchangeServiceResolver
    {
        string GetWebSocketUrl(Exchange exchange);
    }
}
