﻿using OrderBook.Application.Models.Requests;

namespace OrderBook.Application.Abstractions
{
    public interface ISubscriptionManager
    {
        Task AddSubscriptionAsync(SubscribeRequest request);
        Task RemoveSubscriptionAsync(string exchange, string symbol);
        IEnumerable<SubscribeRequest> GetSubscriptions();
        CancellationTokenSource GetCancellationTokenSource(string key);
        void RemoveCancellationTokenSource(string key);
    }
}
