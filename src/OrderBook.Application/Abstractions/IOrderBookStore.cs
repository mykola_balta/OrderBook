﻿using OrderBook.Domain.Models;

namespace OrderBook.Application.Abstractions
{
    public interface IOrderBookStore
    {
        void InitializeOrderBooks(string exchange, string symbol);
        void UpdateOrderBooks(string exchange, string symbol, OrderBookData orderBookData);
        List<Order> GetAsks(string exchange, string symbol, int depth);
        List<Order> GetBids(string exchange, string symbol, int depth);
    }
}
