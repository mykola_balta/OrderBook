﻿using OrderBook.Domain.Models;

namespace OrderBook.Application.Abstractions
{
    public interface INotificationService
    {
        Task NotifyOrderBookUpdate(string exchange, string symbol, List<Order> asks, List<Order> bids);
    }
}
