﻿namespace OrderBook.Application.Abstractions
{
    public interface IWebSocketService
    {
        Task ConnectAsync(string url, CancellationToken cancellationToken);
        Task SendAsync(string message, CancellationToken cancellationToken);
        Task<string> ReceiveAsync(CancellationToken cancellationToken);
        Task DisconnectAsync(CancellationToken cancellationToken);
    }
}
