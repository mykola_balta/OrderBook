﻿using OrderBook.Application.Models.Requests;
using OrderBook.Domain.Models;

namespace OrderBook.Application.Abstractions
{
    public interface IOrderBookService
    {
        Task ReceiveAndProcessMessagesAsync(string exchange, string symbol, int depth, CancellationToken token);
        Task SubscribeAsync(SubscribeRequest request, CancellationToken cancellationToken);
    }
}
