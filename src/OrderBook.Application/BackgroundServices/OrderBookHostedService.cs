﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OrderBook.Application.Abstractions;
using System.Collections.Concurrent;

namespace OrderBook.Application.BackgroundServices
{
    public class OrderBookHostedService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<OrderBookHostedService> _logger;
        private readonly ISubscriptionManager _subscriptionManager;
        private readonly ConcurrentDictionary<string, Task> _processingTasks;
        private const int SubscriptionMonitorDelay = 10000;

        public OrderBookHostedService(IServiceProvider serviceProvider, ILogger<OrderBookHostedService> logger, ISubscriptionManager subscriptionManager)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
            _subscriptionManager = subscriptionManager;
            _processingTasks = new ConcurrentDictionary<string, Task>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await MonitorSubscriptionsAsync(stoppingToken);
        }

        private async Task MonitorSubscriptionsAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var subscriptions = _subscriptionManager.GetSubscriptions();

                foreach (var subscription in subscriptions)
                {
                    var key = $"{subscription.Exchange}_{subscription.Symbol}";

                    if (_processingTasks.ContainsKey(key)) continue;

                    var tokenSource = _subscriptionManager.GetCancellationTokenSource(key);
                    var token = tokenSource.Token;

                    _processingTasks[key] = Task.Run(async () =>
                    {
                        using var scope = _serviceProvider.CreateScope();
                        var orderBookService = scope.ServiceProvider.GetRequiredService<IOrderBookService>();

                        try
                        {
                            await orderBookService.SubscribeAsync(subscription, token);
                            await orderBookService.ReceiveAndProcessMessagesAsync(subscription.Exchange.ToString(), subscription.Symbol, subscription.Depth, token);
                        }
                        catch (OperationCanceledException)
                        {
                            _logger.LogInformation("Subscription for {Exchange} {Symbol} cancelled.", subscription.Exchange, subscription.Symbol);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error processing messages for {Exchange} {Symbol}.", subscription.Exchange, subscription.Symbol);
                        }
                        finally
                        {
                            _processingTasks.TryRemove(key, out _);
                            _subscriptionManager.RemoveCancellationTokenSource(key);
                        }
                    }, token);
                }

                await Task.Delay(SubscriptionMonitorDelay, stoppingToken); // Check for new subscriptions every 10 seconds
            }
        }
    }
}
