﻿using OrderBook.Application.Abstractions;
using OrderBook.Domain.Models;
using System.Collections.Concurrent;
using System.Globalization;

namespace OrderBook.Application.Implementation
{
    public class OrderBookStore : IOrderBookStore
    {
        private readonly ConcurrentDictionary<string, SortedList<decimal, decimal>> _asks;
        private readonly ConcurrentDictionary<string, SortedList<decimal, decimal>> _bids;

        public OrderBookStore()
        {
            _asks = new ConcurrentDictionary<string, SortedList<decimal, decimal>>();
            _bids = new ConcurrentDictionary<string, SortedList<decimal, decimal>>();
        }

        public void InitializeOrderBooks(string exchange, string symbol)
        {
            _asks[GetAsksKey(exchange, symbol)] = new SortedList<decimal, decimal>();
            _bids[GetBidsKey(exchange, symbol)] = new SortedList<decimal, decimal>();
        }

        public void UpdateOrderBooks(string exchange, string symbol, OrderBookData orderBookData)
        {
            var asksKey = GetAsksKey(exchange, symbol);
            var bidsKey = GetBidsKey(exchange, symbol);

            if (orderBookData.Type == "snapshot")
            {
                UpdateSnapshot(asksKey, bidsKey, orderBookData.Data.AsksRaw, orderBookData.Data.BidsRaw);
            }
            else if (orderBookData.Type == "delta")
            {
                UpdateDelta(asksKey, bidsKey, orderBookData.Data.AsksRaw, orderBookData.Data.BidsRaw);
            }
        }

        public List<Order> GetAsks(string exchange, string symbol, int depth) =>
            GetOrders(_asks, GetAsksKey(exchange, symbol), depth);

        public List<Order> GetBids(string exchange, string symbol, int depth) =>
            GetOrders(_bids, GetBidsKey(exchange, symbol), depth);

        private static string GetAsksKey(string exchange, string symbol) => $"{exchange}_{symbol}_asks";
        private static string GetBidsKey(string exchange, string symbol) => $"{exchange}_{symbol}_bids";

        private void UpdateSnapshot(string asksKey, string bidsKey, List<List<string>> asksRaw, List<List<string>> bidsRaw)
        {
            _asks[asksKey].Clear();
            _bids[bidsKey].Clear();

            foreach (var ask in asksRaw)
            {
                _asks[asksKey][ParsePrice(ask)] = ParseSize(ask);
            }

            foreach (var bid in bidsRaw)
            {
                _bids[bidsKey][ParsePrice(bid)] = ParseSize(bid);
            }
        }

        private void UpdateDelta(string asksKey, string bidsKey, List<List<string>> asksRaw, List<List<string>> bidsRaw)
        {
            UpdateOrders(_asks, asksKey, asksRaw);
            UpdateOrders(_bids, bidsKey, bidsRaw);
        }

        private static void UpdateOrders(ConcurrentDictionary<string, SortedList<decimal, decimal>> orders, string key, List<List<string>> rawOrders)
        {
            foreach (var order in rawOrders)
            {
                var price = ParsePrice(order);
                var size = ParseSize(order);

                if (size == 0)
                {
                    orders[key].Remove(price);
                }
                else
                {
                    orders[key][price] = size;
                }
            }
        }

        private static decimal ParsePrice(List<string> order) => decimal.Parse(order[0], CultureInfo.InvariantCulture);
        private static decimal ParseSize(List<string> order) => decimal.Parse(order[1], CultureInfo.InvariantCulture);

        private static List<Order> GetOrders(ConcurrentDictionary<string, SortedList<decimal, decimal>> orders, string key, int depth) =>
            orders.ContainsKey(key)
                ? orders[key].Take(depth).Select(kv => new Order { Price = kv.Key, Size = kv.Value }).ToList()
                : new List<Order>();
    }

}
