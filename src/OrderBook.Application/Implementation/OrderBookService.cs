﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OrderBook.Application.Abstractions;
using OrderBook.Application.Models.Requests;
using OrderBook.Domain.Logging;
using OrderBook.Domain.Models;

namespace OrderBook.Application.Implementation
{
    public class OrderBookService : IOrderBookService
    {
        private readonly IWebSocketService _webSocketService;
        private readonly ILogger<OrderBookService> _logger;
        private readonly IOrderBookStore _orderBookStore;
        private readonly IExchangeServiceResolver _exchangeServiceResolver;
        private readonly INotificationService _notificationService;

        public OrderBookService(
            IWebSocketService webSocketService,
            ILogger<OrderBookService> logger,
            IOrderBookStore orderBookStore,
            IExchangeServiceResolver exchangeServiceResolver,
            INotificationService notificationService)
        {
            _webSocketService = webSocketService;
            _logger = logger;
            _orderBookStore = orderBookStore;
            _exchangeServiceResolver = exchangeServiceResolver;
            _notificationService = notificationService;
        }

        public async Task SubscribeAsync(SubscribeRequest request, CancellationToken cancellationToken)
        {
            string url = _exchangeServiceResolver.GetWebSocketUrl(request.Exchange);
            string subscribeMessage = CreateSubscribeMessage(request.Symbol, request.Depth);

            _orderBookStore.InitializeOrderBooks(request.Exchange.ToString(), request.Symbol);

            await _webSocketService.ConnectAsync(url, cancellationToken);
            await _webSocketService.SendAsync(subscribeMessage, cancellationToken);
        }

        public async Task ReceiveAndProcessMessagesAsync(string exchange, string symbol, int depth, CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                var message = await _webSocketService.ReceiveAsync(token);
                if (message != null)
                {
                    await ProcessMessageAsync(message, exchange, symbol, depth);
                }

                await Task.Delay(1000, token);
            }
        }

        private async Task ProcessMessageAsync(string message, string exchange, string symbol, int depth)
        {
            var orderBookData = JsonConvert.DeserializeObject<OrderBookData>(message);

            if (orderBookData?.Data != null)
            {
                _orderBookStore.UpdateOrderBooks(exchange, symbol, orderBookData);
                var topAsks = _orderBookStore.GetAsks(exchange, symbol, depth);
                var topBids = _orderBookStore.GetBids(exchange, symbol, depth);

                await _notificationService.NotifyOrderBookUpdate(exchange, symbol, topAsks, topBids);

                var snapshotLog = new OrderBookSnapshotLog
                {
                    Exchange = exchange,
                    Symbol = symbol,
                    Asks = topAsks,
                    Bids = topBids,
                    Timestamp = DateTime.UtcNow
                };
                _logger.LogInformation($"Order book updated for {exchange} - {symbol}");
                LogSnapshot(snapshotLog);
            }
        }

        private string CreateSubscribeMessage(string symbol, int depth) =>
            JsonConvert.SerializeObject(new SubscriptionMessage(symbol, depth));

        private void LogSnapshot(OrderBookSnapshotLog log)
        {
            var asksJson = JsonConvert.SerializeObject(log.Asks);
            var bidsJson = JsonConvert.SerializeObject(log.Bids);

            _logger.LogInformation("OrderBookSnapshot: {Exchange}, {Symbol}, {Asks}, {Bids}, {Timestamp}",
                log.Exchange, log.Symbol, asksJson, bidsJson, log.Timestamp);
        }
    }


}
