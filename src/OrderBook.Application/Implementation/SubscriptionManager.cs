﻿using OrderBook.Application.Abstractions;
using OrderBook.Application.Models.Requests;
using System.Collections.Concurrent;

namespace OrderBook.Application.Implementation
{
    public class SubscriptionManager : ISubscriptionManager
    {
        private readonly ConcurrentDictionary<string, SubscribeRequest> _subscriptions;
        private readonly ConcurrentDictionary<string, CancellationTokenSource> _cancellationTokens;

        public SubscriptionManager()
        {
            _subscriptions = new ConcurrentDictionary<string, SubscribeRequest>();
            _cancellationTokens = new ConcurrentDictionary<string, CancellationTokenSource>();
        }

        public Task AddSubscriptionAsync(SubscribeRequest request)
        {
            var key = $"{request.Exchange}_{request.Symbol}";
            if (!_subscriptions.TryAdd(key, request))
            {
                throw new InvalidOperationException("Subscription already exists.");
            }
            return Task.CompletedTask;
        }

        public Task RemoveSubscriptionAsync(string exchange, string symbol)
        {
            var key = $"{exchange}_{symbol}";
            if (_subscriptions.TryRemove(key, out var request))
            {
                if (_cancellationTokens.TryRemove(key, out var tokenSource))
                {
                    tokenSource.Cancel();
                }
            }
            return Task.CompletedTask;
        }

        public IEnumerable<SubscribeRequest> GetSubscriptions()
        {
            return _subscriptions.Values;
        }

        public CancellationTokenSource GetCancellationTokenSource(string key)
        {
            return _cancellationTokens.GetOrAdd(key, _ => new CancellationTokenSource());
        }

        public void RemoveCancellationTokenSource(string key)
        {
            if (_cancellationTokens.TryRemove(key, out var tokenSource))
            {
                tokenSource.Cancel();
            }
        }
    }

}
