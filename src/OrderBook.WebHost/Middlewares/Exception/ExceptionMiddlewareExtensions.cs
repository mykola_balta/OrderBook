﻿namespace OrderBook.WebHost.Middlewares.Exception
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void UseExceptionHandling(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
