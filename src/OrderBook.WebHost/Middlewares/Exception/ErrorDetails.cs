﻿using Newtonsoft.Json;

namespace OrderBook.WebHost.Middlewares.Exception
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; } = null!;
        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
