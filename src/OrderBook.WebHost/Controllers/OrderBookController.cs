﻿using Microsoft.AspNetCore.Mvc;
using OrderBook.Application.Abstractions;
using OrderBook.Application.Models.Requests;
using OrderBook.Domain.Enums;

namespace OrderBook.WebHost.Controllers
{
    [ApiController]
    [Route("api/order-book")]
    public class OrderBookController : ControllerBase
    {
        private readonly ISubscriptionManager _subscriptionManager;

        public OrderBookController(ISubscriptionManager subscriptionManager)
        {
            _subscriptionManager = subscriptionManager;
        }

        [HttpPost("subscribe")]
        public async Task<IActionResult> Subscribe([FromBody] SubscribeRequest request)
        {
            await _subscriptionManager.AddSubscriptionAsync(request);
            return Ok();
        }

        [HttpPost("unsubscribe")]
        public async Task<IActionResult> Unsubscribe([FromBody] UnsubscribeRequest request)
        {
            await _subscriptionManager.RemoveSubscriptionAsync(request.Exchange.ToString(), request.Symbol);
            return Ok();
        }
    }

}
