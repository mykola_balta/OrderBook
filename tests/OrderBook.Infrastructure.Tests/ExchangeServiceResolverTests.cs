﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NSubstitute;
using OrderBook.Domain.Enums;
using OrderBook.Infrastructure.Services;

namespace OrderBook.Infrastructure.Tests
{
    public class ExchangeServiceResolverTests
    {
        private readonly ILogger<ExchangeServiceResolver> _logger;
        private readonly IConfiguration _configuration;

        public ExchangeServiceResolverTests()
        {
            _logger = Substitute.For<ILogger<ExchangeServiceResolver>>();
            _configuration = Substitute.For<IConfiguration>();
        }

        private void FillConfiguration()
        {
            var exchangeUrls = new Dictionary<string, string>
            {
                { "ExchangeUrls:ByBit", "wss://stream.bybit.com/v5/public/spot" },
                { "ExchangeUrls:BitGet", "wss://ws.bitget.com/v2/ws/public" },
                { "ExchangeUrls:Coinbase", "wss://ws-feed.exchange.coinbase.com" }
            };

            foreach (var kvp in exchangeUrls)
            {
                _configuration[kvp.Key].Returns(kvp.Value);
            }
        }
        [Fact]
        public void Constructor_ShouldInitializeCorrectly_WhenAllExchangeUrlsPresent()
        {
            // Arrange
            FillConfiguration();

            // Act
            var resolver = new ExchangeServiceResolver(_logger, _configuration);

            // Assert
            Assert.NotNull(resolver);
        }

        [Fact]
        public void Constructor_ShouldThrowArgumentNullException_WhenExchangeUrlIsMissing()
        {
            // Arrange
            _configuration["ExchangeUrls:ByBit"].Returns((string)null);

            // Act & Assert
            var exception = Assert.Throws<ArgumentNullException>(() => new ExchangeServiceResolver(_logger, _configuration));
            Assert.Equal("Exchange URL for ByBit is missing. (Parameter 'Value')", exception.Message);
        }

        [Fact]
        public void GetWebSocketUrl_ShouldReturnCorrectUrl_ForKnownExchange()
        {
            // Arrange
            var exchangeUrl = "wss://ws.bitget.com/v2/ws/public";
            FillConfiguration();

            var resolver = new ExchangeServiceResolver(_logger, _configuration);

            // Act
            var url = resolver.GetWebSocketUrl(Exchange.BitGet);

            // Assert
            Assert.Equal(exchangeUrl, url);
        }

        [Fact]
        public void GetWebSocketUrl_ShouldThrowArgumentException_ForUnknownExchange()
        {
            // Arrange
            FillConfiguration();
            var resolver = new ExchangeServiceResolver(_logger, _configuration);

            // Act & Assert
            var exception = Assert.Throws<ArgumentException>(() => resolver.GetWebSocketUrl((Exchange)999));
            Assert.Equal("Unknown exchange: 999", exception.Message);
        }
    }
}
