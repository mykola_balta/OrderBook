﻿using Microsoft.Extensions.Logging;
using NSubstitute;
using OrderBook.Application.Abstractions;
using OrderBook.Application.Implementation;
using OrderBook.Application.Models.Requests;
using OrderBook.Domain.Enums;
using OrderBook.Domain.Models;

namespace OrderBook.Application.Tests
{
    public class OrderBookServiceTests
    {
        private readonly IWebSocketService _webSocketService;
        private readonly ILogger<OrderBookService> _logger;
        private readonly IOrderBookStore _orderBookStore;
        private readonly IExchangeServiceResolver _exchangeServiceResolver;
        private readonly INotificationService _notificationService;
        private readonly OrderBookService _orderBookService;

        public OrderBookServiceTests()
        {
            _webSocketService = Substitute.For<IWebSocketService>();
            _logger = Substitute.For<ILogger<OrderBookService>>();
            _orderBookStore = Substitute.For<IOrderBookStore>();
            _exchangeServiceResolver = Substitute.For<IExchangeServiceResolver>();
            _notificationService = Substitute.For<INotificationService>();

            _orderBookService = new OrderBookService(
                _webSocketService,
                _logger,
                _orderBookStore,
                _exchangeServiceResolver,
                _notificationService);
        }

        [Fact]
        public async Task SubscribeAsync_ShouldInitializeOrderBookAndConnectWebSocket()
        {
            // Arrange
            var request = new SubscribeRequest { Exchange = Exchange.ByBit, Symbol = "BTCUSD", Depth = 10 };
            var cancellationToken = new CancellationToken();
            var url = "wss://example.com";
            _exchangeServiceResolver.GetWebSocketUrl(Exchange.ByBit).Returns(url);

            // Act
            await _orderBookService.SubscribeAsync(request, cancellationToken);

            // Assert
            _orderBookStore.Received(1).InitializeOrderBooks(request.Exchange.ToString(), request.Symbol);
            await _webSocketService.Received(1).ConnectAsync(url, cancellationToken);
            await _webSocketService.Received(1).SendAsync(Arg.Any<string>(), cancellationToken);
        }

        [Fact]
        public async Task ReceiveAndProcessMessagesAsync_ShouldProcessReceivedMessages()
        {
            // Arrange
            var exchange = "Binance";
            var symbol = "BTCUSD";
            var depth = 10;
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;

            _webSocketService.ReceiveAsync(cancellationToken).Returns(Task.FromResult("{\"Type\":\"snapshot\",\"Data\":{\"AsksRaw\":[[\"50000.0\",\"1.5\"]],\"BidsRaw\":[[\"49000.0\",\"2.0\"]]}}"));

            // Act
            var task = _orderBookService.ReceiveAndProcessMessagesAsync(exchange, symbol, depth, cancellationToken);
            cancellationTokenSource.Cancel();

            // Assert
            await _notificationService.Received(1).NotifyOrderBookUpdate(
                Arg.Is(exchange),
                Arg.Is(symbol),
                Arg.Any<List<Order>>(),
                Arg.Any<List<Order>>());
        }

        [Fact]
        public async Task ProcessMessageAsync_ShouldUpdateOrderBookAndNotify()
        {
            // Arrange
            var message = "{\"Type\":\"snapshot\",\"Data\":{\"AsksRaw\":[[\"50000.0\",\"1.5\"]],\"BidsRaw\":[[\"49000.0\",\"2.0\"]]}}";
            var exchange = "Binance";
            var symbol = "BTCUSD";
            var depth = 10;

            var asks = new List<Order> { new Order { Price = 50000.0m, Size = 1.5m } };
            var bids = new List<Order> { new Order { Price = 49000.0m, Size = 2.0m } };

            _orderBookStore.GetAsks(exchange, symbol, depth).Returns(asks);
            _orderBookStore.GetBids(exchange, symbol, depth).Returns(bids);

            // Act
            _orderBookService.GetType().GetMethod("ProcessMessageAsync", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                .Invoke(_orderBookService, new object[] { message, exchange, symbol, depth });

            // Assert
            _orderBookStore.Received(1).UpdateOrderBooks(exchange, symbol, Arg.Any<OrderBookData>());
            await _notificationService.Received(1).NotifyOrderBookUpdate(exchange, symbol, asks, bids);
        }
    }
}
