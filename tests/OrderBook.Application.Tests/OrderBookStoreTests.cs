﻿using OrderBook.Application.Implementation;
using OrderBook.Domain.Models;

namespace OrderBook.Application.Tests
{
    public class OrderBookStoreTests
    {
        private readonly OrderBookStore _orderBookStore;

        public OrderBookStoreTests()
        {
            _orderBookStore = new OrderBookStore();
        }

        [Fact]
        public void InitializeOrderBooks_ShouldInitializeEmptyOrderBooks()
        {
            // Arrange
            string exchange = "Binance";
            string symbol = "BTCUSD";

            // Act
            _orderBookStore.InitializeOrderBooks(exchange, symbol);

            // Assert
            var asks = _orderBookStore.GetAsks(exchange, symbol, 10);
            var bids = _orderBookStore.GetBids(exchange, symbol, 10);

            Assert.Empty(asks);
            Assert.Empty(bids);
        }

        [Fact]
        public void UpdateOrderBooks_ShouldUpdateOrderBooksWithSnapshot()
        {
            // Arrange
            string exchange = "Binance";
            string symbol = "BTCUSD";
            _orderBookStore.InitializeOrderBooks(exchange, symbol);

            var orderBookData = new OrderBookData
            {
                Type = "snapshot",
                Data = new OrderBookDataDetails
                {
                    AsksRaw = new List<List<string>> { new List<string> { "50000.0", "1.5" } },
                    BidsRaw = new List<List<string>> { new List<string> { "49000.0", "2.0" } }
                }
            };

            // Act
            _orderBookStore.UpdateOrderBooks(exchange, symbol, orderBookData);

            // Assert
            var asks = _orderBookStore.GetAsks(exchange, symbol, 10);
            var bids = _orderBookStore.GetBids(exchange, symbol, 10);

            Assert.Single(asks);
            Assert.Single(bids);
            Assert.Equal(50000.0m, asks[0].Price);
            Assert.Equal(1.5m, asks[0].Size);
            Assert.Equal(49000.0m, bids[0].Price);
            Assert.Equal(2.0m, bids[0].Size);
        }

        [Fact]
        public void UpdateOrderBooks_ShouldUpdateOrderBooksWithDelta()
        {
            // Arrange
            string exchange = "Binance";
            string symbol = "BTCUSD";
            _orderBookStore.InitializeOrderBooks(exchange, symbol);

            var snapshotData = new OrderBookData
            {
                Type = "snapshot",
                Data = new OrderBookDataDetails
                {
                    AsksRaw = new List<List<string>> { new List<string> { "50000.0", "1.5" } },
                    BidsRaw = new List<List<string>> { new List<string> { "49000.0", "2.0" } }
                }
            };

            _orderBookStore.UpdateOrderBooks(exchange, symbol, snapshotData);

            var deltaData = new OrderBookData
            {
                Type = "delta",
                Data = new OrderBookDataDetails
                {
                    AsksRaw = new List<List<string>> { new List<string> { "50000.0", "0.0" } },
                    BidsRaw = new List<List<string>> { new List<string> { "49000.0", "3.0" } }
                }
            };

            // Act
            _orderBookStore.UpdateOrderBooks(exchange, symbol, deltaData);

            // Assert
            var asks = _orderBookStore.GetAsks(exchange, symbol, 10);
            var bids = _orderBookStore.GetBids(exchange, symbol, 10);

            Assert.Empty(asks);
            Assert.Single(bids);
            Assert.Equal(49000.0m, bids[0].Price);
            Assert.Equal(3.0m, bids[0].Size);
        }

        [Fact]
        public void GetAsks_ShouldReturnCorrectAsks()
        {
            // Arrange
            string exchange = "Binance";
            string symbol = "BTCUSD";
            _orderBookStore.InitializeOrderBooks(exchange, symbol);

            var orderBookData = new OrderBookData
            {
                Type = "snapshot",
                Data = new OrderBookDataDetails
                {
                    AsksRaw = new List<List<string>> { new List<string> { "50000.0", "1.5" }, new List<string> { "50500.0", "2.0" } },
                    BidsRaw = new List<List<string>> { new List<string> { "49000.0", "2.0" }, new List<string> { "48500.0", "1.0" } }
                }
            };

            _orderBookStore.UpdateOrderBooks(exchange, symbol, orderBookData);

            // Act
            var asks = _orderBookStore.GetAsks(exchange, symbol, 1);

            // Assert
            Assert.Single(asks);
            Assert.Equal(50000.0m, asks[0].Price);
            Assert.Equal(1.5m, asks[0].Size);
        }

        [Fact]
        public void GetBids_ShouldReturnCorrectBids()
        {
            // Arrange
            string exchange = "Binance";
            string symbol = "BTCUSD";
            _orderBookStore.InitializeOrderBooks(exchange, symbol);

            var orderBookData = new OrderBookData
            {
                Type = "snapshot",
                Data = new OrderBookDataDetails
                {
                    AsksRaw = new List<List<string>> { new List<string> { "50000.0", "1.5" }, new List<string> { "50500.0", "2.0" } },
                    BidsRaw = new List<List<string>> { new List<string> { "49000.0", "2.0" }, new List<string> { "48500.0", "1.0" } }
                }
            };

            _orderBookStore.UpdateOrderBooks(exchange, symbol, orderBookData);

            // Act
            var bids = _orderBookStore.GetBids(exchange, symbol, 2);

            // Assert
            Assert.Equal(2, bids.Count);
            Assert.Equal(48500.0m, bids[0].Price);
            Assert.Equal(1.0m, bids[0].Size);
            Assert.Equal(49000.0m, bids[1].Price);
            Assert.Equal(2.0m, bids[1].Size);
        }
    }
}
