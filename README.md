## Order book depth Chart Application

This application visualizes the bid and ask order book data for trading platforms, showing real-time data in a dynamic and interactive chart

![Latest Animated Chart](resources/animated_chart3.gif)

![Animated Chart](resources/animated_chart.gif)
![Animated Chart2](resources/animated_chart2.gif)


## Features:

It uses ASP.NET Core to serve and stream order book data via SignalR to a React.js frontend, which displays the data on an interactive chart built with Plotly.js. The application also features comprehensive logging with ELK (Elasticsearch, Logstash, Kibana) to monitor and analyze bids and asks over time. 

## Stack:
- .Net 8
- React (TypeScript)
- SignalR
- ELK Stack
- Plotly.js

## Installing:

Docker for a quick and easy start, or by manually setting up the backend and frontend services.

### Using Docker:

    from root directory
    
    docker-compose up

### Manual Setup:
    cd  src\OrderBook.WebHost
    dotnet restore
    dotnet run

    index for ELK: "orderbook-audit-log-2024-06"
    
    cd orderbook-webapp
    npm install
    npm start

## Points for improvement:
- Add ability to select different exchanges and currency pairs dynamically.
- Add ability for clients to subscribe and unsubscribe to order book changes based on their current interest, reducing data transfer where not needed.
- Replace the logging system to a serverless architecture, which uses triggers from a message queue to handle log entries. Or use the No-Sql database
- Improve charting options and improve the overall UI/UX design.
- ...

![tests](resources/tests.png)

![ELK](resources/ELK.png)

